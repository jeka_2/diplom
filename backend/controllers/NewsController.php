<?php

namespace backend\controllers;

use yii2tech\admin\CrudController;

/**
 * NewsController implements the CRUD actions for [[common\models\News]] model.
 * @see common\models\News
 */
class NewsController extends CrudController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\News';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'backend\models\NewsSearch';
}
