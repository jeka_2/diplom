<?php

namespace backend\controllers;

use yii2tech\admin\CrudController;

/**
 * UserController implements the CRUD actions for [[common\models\User]] model.
 * @see common\models\User
 */
class UserController extends CrudController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\User';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'backend\models\UserSearch';
}
