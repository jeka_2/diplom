<?php
namespace backend\controllers;

use common\models\Auth;
use common\models\User;
use Yii;
use yii\authclient\ClientInterface;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'auth'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param ClientInterface $client
     * @return boolean|Response
     */
    public function oAuthSuccess($client) {
        // get user data from client

        $userAttributes = $client->getUserAttributes();

        $user = User::findByEmail($userAttributes["email"]);

        if ($user) {
            Yii::$app->user->login($user, 3600 * 24 * 30);
        } else {
            $user = new User();
            $user->email = $userAttributes["email"];
            $user->username = explode("@", $user->email)[0];
            $user->generatePasswordResetToken();
            $user->setPassword(rand(1000000, 100000000));
            $user->save();
        }

        $source = $client->getName();
        $sourceId = $userAttributes["id"];
        $uid = $user->id;
        $auth = Auth::find()->where([
            "user_id"=>$uid,
            "source"=>$source
        ])->one();

        if (!$auth) {
            $auth = new Auth();
        }

        $auth->user_id = $uid;
        $auth->source = $source;
        $auth->source_id = $sourceId;
        $auth->save();

        return $this->goHome();
    }

}
