<?php

namespace backend\controllers;

use yii2tech\admin\CrudController;

/**
 * TagsController implements the CRUD actions for [[common\models\Tags]] model.
 * @see common\models\Tags
 */
class TagsController extends CrudController
{
    /**
     * @inheritdoc
     */
    public $modelClass = 'common\models\Tags';
    /**
     * @inheritdoc
     */
    public $searchModelClass = 'backend\models\TagsSearch';
}
