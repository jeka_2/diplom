<?php

namespace backend\controllers;

use yii2tech\admin\CrudController;

/**
 * TemplatesController implements the CRUD actions for [[\common\models\Templates]] model.
 * @see \common\models\Templates
 */
class TemplatesController extends CrudController
{
    /**
     * @inheritdoc
     */
    public $modelClass = '\common\models\Templates';
    /**
     * @inheritdoc
     */
    public $searchModelClass = '\backend\models\TemplatesSearch';
}
