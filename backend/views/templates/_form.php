<?php

use yii\bootstrap\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Templates */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="row">
    <div class="col-lg-5">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'data')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'source_url')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'text_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'link_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'title_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tag_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'tags_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'pic_template')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'remove_templates')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>