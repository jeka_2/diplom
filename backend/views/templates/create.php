<?php

/* @var $this yii\web\View */
/* @var $model common\models\Templates */

$this->title = 'Create Templates';
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

