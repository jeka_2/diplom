<?php

use yii\bootstrap\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Templates */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Templates', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['update', 'id' => $model->id],
    ['delete', 'id' => $model->id]
];
?>
<div class="row">
    <div class="col-lg-8 detail-view-wrap">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
//            'data:ntext',
            'created_at:date',
            'updated_at:date',
            'url:url',
            'source_url',
//            'link_template',
//            'tag_template',
//            'tags_template',
//            'text_template',
//            'title_template',
//            'remove_templates:ntext',
        ],
    ]) ?>
    </div>
</div>