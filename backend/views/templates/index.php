<?php

use yii\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TemplatesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Templates';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create']
];
?>

<p class="text-right">
    <a class="btn btn-warning" href="<?= \yii\helpers\Url::to(['templates/create'])?>"><?= Yii::t("main", "create")?></a>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'data:ntext',
        'created_at',
        'updated_at',
        'url:url',
//         'link_template',
//         'tag_template',
//         'tags_template',
//         'source_url',
//         'text_template',
//         'title_template',
//         'remove_templates:ntext',

        [
            'class' => ActionColumn::className(),
        ],
    ],
]); ?>
