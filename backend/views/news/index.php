<?php

use yii\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'News';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create']
];
?>

<p class="text-right">
    <a class="btn btn-warning" href="<?= \yii\helpers\Url::to(['news/create'])?>"><?= Yii::t("main", "create")?></a>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'lang',
        'title',
        'short:ntext',
        'data:ntext',
        'pic',
        // 'created_at',
        // 'updated_at',
        // 'views',
        // 'original_urls:ntext',

        [
            'class' => ActionColumn::className(),
        ],
    ],
]); ?>
