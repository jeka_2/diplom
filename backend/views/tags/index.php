<?php

use yii\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\TagsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tags';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create']
];
?>


<p class="text-right">
    <a class="btn btn-warning" href="<?= \yii\helpers\Url::to(['tags/create'])?>"><?= Yii::t("main", "create")?></a>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'name',
        'created_at',
        'updated_at',
        'times:datetime',

        [
            'class' => ActionColumn::className(),
        ],
    ],
]); ?>
