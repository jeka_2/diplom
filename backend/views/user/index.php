<?php

use yii\grid\GridView;
use yii2tech\admin\grid\ActionColumn;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
$this->params['contextMenuItems'] = [
    ['create']
];
?>


<p class="text-right">
    <a class="btn btn-warning" href="<?= \yii\helpers\Url::to(['user/create'])?>"><?= Yii::t("main", "create")?></a>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'username',
        'auth_key',
        'password_hash',
        'password_reset_token',
        // 'email:email',
        // 'status',
        // 'created_at',
        // 'updated_at',
        // 'lang',

        [
            'class' => ActionColumn::className(),
        ],
    ],
]); ?>
