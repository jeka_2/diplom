<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Templates;

/**
 * TemplatesSearch represents the model behind the search form about `\common\models\Templates`.
 */
class TemplatesSearch extends Templates
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['data', 'url', 'source_url', 'link_template', 'tag_template', 'tags_template', 'text_template', 'title_template', 'remove_templates'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Templates::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'source_url', $this->source_url])
            ->andFilterWhere(['like', 'link_template', $this->link_template])
            ->andFilterWhere(['like', 'text_template', $this->text_template])
            ->andFilterWhere(['like', 'title_template', $this->title_template])
            ->andFilterWhere(['like', 'remove_templates', $this->remove_templates])
            ->andFilterWhere(['like', 'tag_template', $this->remove_templates])
            ->andFilterWhere(['like', 'tags_template', $this->remove_templates]);

        return $dataProvider;
    }
}
