<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            'generators' => [
                'adminMainFrame' => [
                    'class' => 'yii2tech\admin\gii\mainframe\Generator'
                ],
                'adminCrud' => [
                    'class' => 'yii2tech\admin\gii\crud\Generator'
                ]
            ],
        ],

    ],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            // Hide index.php
            'showScriptName' => false,
            // Use pretty URLs
            'enablePrettyUrl' => true,
            'rules' => [
                '/' => 'site/index',
                'templates' => 'templates/index',
                'templates/<view:[\w\-]+>' => 'templates/<view>',
                'news' => 'news/index',
                'news/<view:[\w\-]+>' => 'news/<view>',

                '<view:[\w\-]+>' => 'site/<view>',
                '<view:[\w\-]+>/<id:[\d]+>' => 'site/<view>',
            ],
        ],


    ],
    'params' => $params,
];
