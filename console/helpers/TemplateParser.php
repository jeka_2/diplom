<?php

namespace console\helpers;

use common\models\News;
use common\models\Tags;
use common\models\TagsToNews;
use common\models\Templates;

class TemplateParser {

    public static function  parseTemplate(Templates $template) {

        $url = $template->source_url;

        $content = file_get_contents($url);

        $regex = $template->remove_templates;


        //parse with regex
        preg_match_all($regex, $content, $matches, PREG_PATTERN_ORDER);


        foreach ($matches[0] as $match) {
            $article = new News();

            preg_match_all($template->title_template, $match, $titleMatches, PREG_PATTERN_ORDER);
            preg_match_all($template->text_template, $match, $textMatches, PREG_PATTERN_ORDER);
            preg_match_all($template->tags_template, $match, $tagsMatches, PREG_PATTERN_ORDER);
            preg_match_all($template->link_template, $match, $linkMatches, PREG_PATTERN_ORDER);
            preg_match_all($template->pic_template, $match, $picMatches, PREG_PATTERN_ORDER);



            $article->title = $titleMatches[1][0] ?? 'unknown';
            $article->data = $textMatches[1][0] ?? 'unknown';
            $article->lang = 'bg';
            $article->short = substr( $article->data, 0, 30);
            $article->pic = $picMatches[1][0] ?? 'img/1920x1080/01-old.jpg';
            $article->original_urls = json_encode([$linkMatches[1][0] ?? $url]);

            if (News::findOne(['title' => $article->title])) {
                continue;
            }

            $article->save();

            foreach ($tagsMatches[1] as $tagName) {

                $tag = Tags::findOne(['name' => $tagName]);
                if (!$tag) {
                    $tag = new Tags();
                    $tag->name = $tagName;
                    $tag->times = 1;
                } else {
                    $tag->times++;
                }
                $tag->save();

                $tagToArticle = new TagsToNews();
                $tagToArticle->n_id = $article->id;
                $tagToArticle->t_id = $tag->id;
                $tagToArticle->save();
            }



        }






    }

}