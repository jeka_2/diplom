<?php

namespace console\controllers;

use common\models\Templates;
use console\helpers\TemplateParser;
use yii\console\Controller;

class ParserController extends Controller {

    public function actionIndex() {
        $templates = Templates::find()->all();

        foreach ($templates as $template) {
            TemplateParser::parseTemplate($template);
        }


    }


}