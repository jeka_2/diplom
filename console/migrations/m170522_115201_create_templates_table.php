<?php

use yii\db\Migration;

/**
 * Handles the creation of table `templates`.
 */
class m170522_115201_create_templates_table extends Migration
{
    const TABLE_NAME = "{{%templates}}";

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
