<?php

use yii\db\Migration;

class m170904_183405_template_to_news extends Migration
{

    const TABLE_NAME = "templates_to_news";

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            't_id' => $this->integer(),
            'n_id' => $this->integer(),
        ]);

        $this->addForeignKey('fk-tpn-id-news_id', self::TABLE_NAME, 'n_id', 'news', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tpn-id-tags_id', self::TABLE_NAME, 't_id', 'templates', 'id', 'CASCADE', 'CASCADE');
        $this->dropColumn("{{%news}}", "from");
    }


    public function down()
    {
        $this->addColumn("{{%news}}", "from", $this->string());
        $this->dropTable(self::TABLE_NAME);
    }
}
