<?php

use yii\db\Migration;

class m170901_081142_lang extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%user}}', 'lang', $this->string(5)->defaultValue('ru-RU'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%user}}', 'lang');
    }
}
