<?php

use yii\db\Migration;

/**
 * Handles the creation of table `news`.
 */
class m170522_115134_create_news_table extends Migration
{
    const TABLE_NAME = "{{%news}}";

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'from' => $this->string(),
            'pic' => $this->string(),
            'lang' => $this->string(2)->defaultValue("ru"),
            'tags' => $this->string(),
            'title' => $this->string(),
            'short' => $this->text(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
