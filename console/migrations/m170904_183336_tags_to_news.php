<?php

use yii\db\Migration;

class m170904_183336_tags_to_news extends Migration
{

    const TABLE_NAME = "tags_to_news";

    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            't_id' => $this->integer(),
            'n_id' => $this->integer(),
        ]);

        $this->addForeignKey('fk-tn-id-news_id', self::TABLE_NAME, 'n_id', 'news', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tn-id-tags_id', self::TABLE_NAME, 't_id', 'tags', 'id', 'CASCADE', 'CASCADE');
        $this->dropColumn("{{%news}}", "tags");
    }


    public function down()
    {
        $this->addColumn("{{%news}}", "tags", $this->string());
        $this->dropTable(self::TABLE_NAME);
    }
}
