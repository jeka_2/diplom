<?php

use yii\db\Migration;

class m170905_143323_tags_ai extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%templates}}', 'tags_template', $this->string()->defaultValue('/class="post-item__tags-item">(<a|<\\\/a>|[^<]+)*/'));
        $this->addColumn('{{%templates}}', 'tag_template', $this->string()->defaultValue('/<[^>]*>(.*)<[^<]*/'));
        $this->addColumn('{{%tags}}', 'times', $this->integer()->defaultValue(0));
        $this->addColumn('{{%ai}}', 'tmn_id', $this->integer());
        $this->addColumn('{{%ai}}', 'tmx_id', $this->integer());
        $this->addColumn('{{%ai}}', 'times', $this->integer()->defaultValue(0));
        $this->addForeignKey('fk-tpn-id-ai_t_id', 'ai', 'tmn_id', 'tags', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-tpn-id-ai_t2_id', 'ai', 'tmx_id', 'tags', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-tpn-id-ai_t_id', '{{%ai}}');
        $this->dropForeignKey('fk-tpn-id-ai_t2_id', '{{%ai}}');
        $this->dropColumn('{{%templates}}', 'tag_template');
        $this->dropColumn('{{%templates}}', 'tags_template');
        $this->dropColumn('{{%tags}}', 'times');
        $this->dropColumn('{{%ai}}', 'times');
        $this->dropColumn('{{%ai}}', 'tmn_id');
        $this->dropColumn('{{%ai}}', 'tmx_id');
    }
}
