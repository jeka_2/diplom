<?php

use yii\db\Migration;

class m170901_111756_template_change extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%templates}}', 'url', $this->string());
        $this->addColumn('{{%templates}}', 'link_template', $this->string());
        $this->addColumn('{{%templates}}', 'text_template', $this->string());
        $this->addColumn('{{%templates}}', 'title_template', $this->string());
        $this->addColumn('{{%templates}}', 'pic_template', $this->string());
        $this->addColumn('{{%templates}}', 'remove_templates', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%templates}}', 'url');
        $this->dropColumn('{{%templates}}', 'link_template');
        $this->dropColumn('{{%templates}}', 'text_template');
        $this->dropColumn('{{%templates}}', 'title_template');
        $this->dropColumn('{{%templates}}', 'pic_template');
        $this->dropColumn('{{%templates}}', 'remove_templates');
    }
}
