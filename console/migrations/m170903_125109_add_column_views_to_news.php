<?php

use yii\db\Migration;

class m170903_125109_add_column_views_to_news extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%news}}', 'views', $this->integer()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%news}}', 'views');
    }
}
