<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tags`.
 */
class m170522_115125_create_tags_table extends Migration
{
    const TABLE_NAME = "{{%tags}}";

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
