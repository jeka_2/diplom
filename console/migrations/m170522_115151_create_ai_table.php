<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ai`.
 */
class m170522_115151_create_ai_table extends Migration
{
    const TABLE_NAME = "{{%ai}}";
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'data' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
