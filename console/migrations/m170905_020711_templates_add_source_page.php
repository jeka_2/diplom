<?php

use yii\db\Migration;

class m170905_020711_templates_add_source_page extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('{{%templates}}', 'source_url', $this->string());
        $this->addColumn('{{%news}}', 'original_urls', $this->text());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('{{%templates}}', 'source_url');
        $this->dropColumn('{{%news}}', 'original_urls');
    }
}
