<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class IndexAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700',
        'css/simple-line-icons.min.css',
        'css/bootstrap.min.css',
        'css/animate.css',
        'css/swiper.min.css',
        'css/layout.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/jquery.min.js',
        'js/jquery-migrate.min.js',
        'js/bootstrap.min.js',
        'js/jquery.easing.js',
        'js/jquery.back-to-top.js',
        'js/jquery.smooth-scroll.js',
        'js/jquery.wow.min.js',
        'js/swiper.jquery.min.js',
        'js/jquery.masonry.pkgd.min.js',
        'js/imagesloaded.pkgd.min.js',
        'js/layout.min.js',
        'js/components/wow.min.js',
        'js/components/swiper.min.js',
        'js/components/masonry.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
