<?php
namespace frontend\controllers;

use common\models\Auth;
use common\models\Feedback;
use common\models\News;
use common\models\Tags;
use common\models\Templates;
use common\models\User;
use frontend\models\SearchForm;
use frontend\models\TemplateGrapper;
use frontend\models\UserEditForm;
use phpDocumentor\Reflection\DocBlock\Tag;
use Yii;
use yii\authclient\ClientInterface;
use yii\base\InvalidParamException;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Site controller
 */
class SiteController extends Controller
{
    const EVENT_BEFORE_FEEDBACK_SAVE = "before_feedback_send";
    const EVENT_GET_FEED_MODEL = "get_feedback";

    /**
     * @var $feedModel Feedback
     */
    public $feedModel= null;


    public function init() {
        $this->on(self::EVENT_GET_FEED_MODEL, function() {
            Yii::$app->view->params['feedBackModel'] = $this->feedModel;
        });
        $this->on(self::EVENT_BEFORE_FEEDBACK_SAVE, function() {
            $model = new Feedback();

            if ($model->load(Yii::$app->request->post())) {
                if ($model->validate()) {
                    // form inputs are valid, do something here
                    $model->save();
                }
            }
            $this->feedModel = $model;
        });
    }



    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],
        ];
    }

    /**
     * @param \yii\base\Action $event
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($event)
    {

        $this->trigger(self::EVENT_BEFORE_FEEDBACK_SAVE);
        if (!$this->feedModel->hasErrors() && !$this->feedModel->isNewRecord) {
            $this->refresh();
            return true;
        }

        return parent::beforeAction($event);
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $this->layout = "index";

        $news = News::find()->orderBy('updated_at desc, id asc')->limit(3)->all();


        return $this->render('index', ['news'=>$news]);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        return $this->render('contact');
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogIn()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogOut()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignUp()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }


    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * This function will be triggered when user is successfuly authenticated using some oAuth client.
     *
     * @param ClientInterface $client
     * @return boolean|Response
     */
    public function oAuthSuccess($client) {
        // get user data from client

        $userAttributes = $client->getUserAttributes();

        $user = User::findByEmail($userAttributes["email"]);

        if ($user) {
            Yii::$app->user->login($user, 3600 * 24 * 30);
        } else {
            $user = new User();
            $user->email = $userAttributes["email"];
            $user->username = explode("@", $user->email)[0];
            $user->generatePasswordResetToken();
            $user->setPassword(rand(1000000, 100000000));
            $user->save();
        }

        $source = $client->getName();
        $sourceId = $userAttributes["id"];
        $uid = $user->id;
        $auth = Auth::find()->where([
            "user_id"=>$uid,
            "source"=>$source
        ])->one();

        if (!$auth) {
            $auth = new Auth();
        }

        $auth->user_id = $uid;
        $auth->source = $source;
        $auth->source_id = $sourceId;
        $auth->save();

        return $this->goHome();
    }


    function actionArticle($id) {
        $article = News::findOne(["id"=>$id]);
        $article->views++;
        $article->save();

        if (!$article) {
            throw new NotFoundHttpException();
        }
        return $this->render('article', ["article"=>$article]);
    }


    function actionNews() {

        return $this->render('news');
    }

    function actionSearch() {

        $model = new SearchForm();

        $renderLoader = false;
        $model->loadData();
        $model->strict = false;
        if (strlen($model->query)) {
            $renderLoader = true;
        }

        return $this->render('search', ["model"=>$model, 'renderLoader'=>$renderLoader]);
    }


    function actionTags($tag=null) {

        $model = new SearchForm();
        $model->loadData();


        $renderLoader = false;
        if ($tag) {
            $model->tags = $tag;
            $renderLoader = true;
        }

        $randomTags = Tags::find()
            ->orderBy(new Expression('rand()'))
            ->limit(30)->all();

        return $this->render('tags', ["model"=>$model,
            'renderLoader'=>$renderLoader,
            'tag' => $tag,
            'tags'=>$randomTags] );
    }


    function actionRender4News($start) {
        $query = Yii::$app->request->get('query', '');
        $sites = Yii::$app->request->get('sites', '');
        $tags = Yii::$app->request->get('tags', '');
        $strict = Yii::$app->request->get('strict', false);


        $news = SearchForm::search($start, 4, $query, $sites, $tags, $strict);
        return json_encode([
            "content"=>$this->renderPartial('parts/news4', ['news'=>$news, 'start'=>$start]),
            "count"=>sizeof($news),
            ]);
    }


    function actionRender3News($start) {

        $query = Yii::$app->request->get('query', '');
        $sites = Yii::$app->request->get('tags', '');
        $tags = Yii::$app->request->get('sites', '');
        $strict = Yii::$app->request->get('strict', true);


        $news = SearchForm::search($start, 3, $query, $sites, $tags, $strict);

        if (!$news) {

            return json_encode([
                "content"=>'',
                "count"=>0
            ]);

        }

        return json_encode([
            "content"=>$this->renderPartial('parts/news3', ['news'=>$news, 'start'=>$start]),
            "count"=>sizeof($news)
        ]);
    }

    function actionCabinet() {


        $model = new UserEditForm();

        $model->username = Yii::$app->user->identity->username;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->edit()) {
                // form inputs are valid, do something here
                return $this->refresh();
            }
        }

        return $this->render('cabinet', [
            'model' => $model,
        ]);
    }


    function actionCron() {
        foreach (Templates::find()->all() as $template) {
            TemplateGrapper::grapAllFromTemplate($template);
        }
    }
}
