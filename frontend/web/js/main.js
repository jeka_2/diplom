jQuery(function($) {
    $('[data-toggle=load_more]').each(function() {
        var elem = $(this);

        var loaderCode = "<div class='icon-loop load_more_loader'><p>Loading...</p></div>";
        var errorCode = "<div class='icon-bag text-danger load_more_loader'><p>Some error1</p></div>";

        $(loaderCode).appendTo(elem);
        var loader = elem.find('.load_more_loader');


        var url = elem.data("url");
        var stop  = false;
        var loading = false;
        function check() {
            if (stop || loading) return;
            loading = true;
            var start = elem.data('start');
            if (!start) start = 0;

            var params = elem.data('params');
            if (!params) {
                params = {};
            }

            var linePos = elem.height() + elem.offset().top;
            var winPos = $(window).height() + $(window).scrollTop();
            if (linePos <= winPos + 200) {
                loader.show();
                $.ajax(elem.data('url'), {
                    data: $.extend({
                        start: start,

                    }, params),
                    method: "GET",
                    dataType: "json",
                    success: function(data) {
                        loading = false;
                        if (!data.count) {
                            stop = true;
                        }
                        $(data.content).insertBefore(loader);
                        elem.data("start",start+data.count);
                        loader.hide();
                    },
                    error: function(error) {
                        loading = false;
                        stop = true;
                        loader.replaceWith(errorCode);
                        console.log(error);
                    }

                });
            } else {
                loading = false;
            }
        }

        check();
        $(window).on('scroll', check);

    });

    $("form[method=get]").submit(function() {
        var form = $(this);
        form.find('input').each(function() {
            var el = $(this);
            if (el.val() == "") {
                el.prop('disabled', true);
            }
        });
        return true;
    });


    $('#tags').each(function() {

        function randInt(mx) {
            return parseInt(Math.random()*10000)%mx;
        }

        function randomColor() {
            var str = "67890abcd";
            var code = "#";
            for (var i = 0; i<6; i++) {
                code += str[randInt(str.length)];
            }
            return code;
        }

        var cnt = $(this);
        var h = cnt.height(), w = cnt.width();

        function moveTags() {
            if (cnt.is(':hover')) return;
            cnt.find('.tag').each(
                function() {
                    var tag = $(this);

                    var circles= randInt(4)-2;

                    tag.css({
                        'color': randomColor(),
                        'top': randInt(h - 100-tag.height()) + 50,
                        'left': randInt(w - 100-tag.width()) + 50,
                        'transform': 'rotate3d(1,1,1,'+(circles*360 + randInt(40)-20)+'deg) skewX('+(randInt(40)-20)+'deg)'
                    });
                }

            );
        }

        moveTags();
        setInterval(moveTags, 3000);

    })
});