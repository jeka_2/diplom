<?php
namespace frontend\models;

use common\models\News;
use common\models\Tags;
use common\models\TagsToNews;
use common\models\Templates;
use common\models\TemplatesToNews;
use Yii;
use yii\base\Model;
use common\models\User;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Signup form
 */
class SearchForm extends Model
{
    public $query;
    public $sites;
    public $tags;
    public $strict = true;


    public function loadData() {
        foreach ($this->attributes as $attr=>$value) {
            $this->$attr = Yii::$app->request->get($attr, "");
        }
    }



    public function getParamsForLoader() {
        return json_encode([
            "query"=>$this->query,
            "tags"=>$this->tags,
            "sites"=>$this->sites,
            'strict'=>$this->strict
        ]);
    }

    /**
     * @param $start
     * @param $limit
     * @param $query
     * @param $sites
     * @param $tags
     * @param $strict
     * @return News[]
     */
    public static function search($start, $limit, $query, $sites, $tags, $strict)
    {


        $ac = News::find();


        /**QUERY**/
        if (strlen($query) && sizeof($parts = preg_split("/[ ,.:-]/", $query))) {
//            var_dump("here");
            $params = ['or'];
            $params [] = ['or like', 'title', $parts];
            $params [] = ['or like', 'data', $parts];
            $ac->where($params);
        }


        /**SITES**/
        $siteParts =  explode(',', str_replace(' ', '', $sites));

        if (strlen($sites) && sizeof($siteParts)) {
            $qParams = [];
            foreach ($siteParts as $part) {
                $qParams []= '%'.$part;
            }



            $ns = TemplatesToNews::find()
                ->innerJoinWith('n')
                ->innerJoinWith('t')
                ->where(['or like', Templates::tableName().'.url', $qParams, false])
                ->all();


            $ids = ArrayHelper::getColumn($ns, 'n_id');

            $ac->andWhere(['id'=>$ids]);
        }


        /**TAGS**/
        $tagParts =  explode(',', str_replace(' ', '', $tags));

        if (strlen($tags) && sizeof($tagParts)) {

            $params = [];
            if (filter_var($strict, FILTER_VALIDATE_BOOLEAN)) {
                $params = [Tags::tableName() . '.name' => $tagParts];
            } else {
                $like = [];
                foreach ($tagParts as $part) {
                    $like []= '%'.$part;
                    $like []= $part.'%';
                }
                $params = ['or like', Tags::tableName().'.name', $like, false];
            }

            $ns = TagsToNews::find()
                ->innerJoinWith('n')
                ->innerJoinWith('t')
                ->where($params)
                ->all();


            $ids = ArrayHelper::getColumn($ns, 'n_id');

            $ac->andWhere(['id'=>$ids]);
        }



        return $ac
            ->orderBy('updated_at desc')
            ->limit($limit)
            ->offset($start)
            ->all();
    }

}
