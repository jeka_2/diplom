<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class UserEditForm extends Model
{
    public $username;
    public $password;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('fields', 'username'),
            'password' => Yii::t('fields', 'password'),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.', 'filter' => ['<>', 'id', Yii::$app->user->id]],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function edit()
    {
        if (!$this->validate()) {
            return null;
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;

        if ($user->username !== $this->username) {
            $user->username = $this->username;
        }

        if ($this->password) {
            $user->setPassword($this->password);
        }

        return $user->save() ? $user : null;
    }
}
