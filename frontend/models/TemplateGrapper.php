<?php

namespace frontend\models;

use common\models\AI;
use common\models\News;
use common\models\Tags;
use common\models\TagsToNews;
use common\models\Templates;
use common\models\TemplatesToNews;
use LanguageDetection\Language;
use phpDocumentor\Reflection\DocBlock\Tag;
use yii\base\Model;
use yii\db\Query;

class TemplateGrapper extends Model {

    static $detector = null;
    static $tagVals = null, $tagCount = null;
    const MAX_GRAP = 20;

    /**
     * @param $template Templates
     */
    public static function grapAllFromTemplate($template) {

        $content = @file_get_contents($template->source_url);
        if (!$content) return;


        $matches = [];
        preg_match_all($template->link_template, $content, $matches);
        $urls = $matches[1];

        foreach ($urls as $url) {

            try {
                self::loadArticle($template, $url);
            } catch (\Exception $e) {
                var_dump($e);
            }
        }

    }

    /**
     * @return Language
     */
    public static function getDetector() {
        if (self::$detector == null) {
            self::$detector = new Language(['ru', 'en', 'bg']);
        }
        return self::$detector;
    }

    /**
     * @param $template Templates
     * @param $url string
     */
    public static function loadArticle($template, $url) {

        $exists = News::find()->where(['or',
            ['like', 'original_urls', '%'.$url, false],
            ['like', 'original_urls', '%'.$url.",", false]
        ])->all();

        //already exists
        if (sizeof($exists)) return;


        usleep(2000);
        $content = file_get_contents($url);

        $matches = [];
        preg_match_all($template->data, $content, $matches);
        $pic = $matches[1][0];

        $matches = [];
        preg_match_all($template->text_template, $content, $matches);
        $text = $matches[1][0];


        $tags = [];
        $matches = [];

        preg_match_all($template->tags_template, $content, $matches);
        foreach ($matches[0] as $tagText) {
            $matches = [];
            preg_match_all($template->tag_template, $tagText, $matches);
            $tags = array_merge($tags, $matches[1]);
        }



        $tagVals = [];
        $tagCount= [];
        $tagObjs = [];
        foreach($tags as $name) {
            $name = mb_strtolower($name);
            if (!($tag = Tags::find()->where(['name'=>$name])->one())) {
                $tag = new Tags();
                $tag->name = $name;
                $tag->save();
            }
            $tagObjs[]= $tag;
            $allTags = Tags::find()->where(['not in', 'name', $tags])->all();
            foreach ($allTags as $tagC) {

                if ($pair = AI::find()->where(['tmn_id'=>$tag->id, 'tmx_id'=>$tagC->id])->one()) {
                    /**
                     * @var $pair AI
                     * @var $tagC Tags
                     */
                    if (!array_key_exists($pair->tmx_id, $tagVals)) {
                        $tagVals[$pair->tmx_id] = 0;
                        $tagCount[$pair->tmx_id] = 0;
                    }
                    $tagVals[$pair->tmx_id] += $tagC->times;
                    $tagCount[$pair->tmx_id] += 1;
                }
            }
        }

        $tagIds = array_keys($tagVals);

        self::$tagVals = $tagVals;
        self::$tagCount = $tagCount;

        usort($tagIds, function ($id1, $id2) {

            $d1 = self::$tagVals[$id1]/self::$tagCount[$id1];
            $d2 = self::$tagVals[$id2]/self::$tagCount[$id2];

            if ($d1 < $d2) {
                return 0;
            }

            return $d1 > $d2;
        });


        for($i=0; $i<min(sizeof($tagIds),3); $i++) {
            $tagObjs[] = Tags::find()->where(['id'=>$tagIds[$i]])->one();
        }

        foreach ($tagObjs as $tag1) {
            /**
             * @var $tag1 Tags
             */
            $tag1->times += 1;
            $tag1->save();


            foreach ($tagObjs as $tag2) {
                if ($tag1->id == $tag2->id) {
                    continue;
                }

                if (!($pair = AI::find()->where(['tmn_id'=>$tag1->id, 'tmx_id'=>$tag2->id])->one())) {
                    $pair = new AI();
                    $pair->tmn_id = $tag1->id;
                    $pair->tmx_id = $tag2->id;
                    $pair->times = 0;
                }
                $pair->times++;
                $pair->save();
            }
        }



        $matches = [];
        preg_match_all($template->title_template, $content, $matches);
        $title = $matches[1][0];


        if (strlen($template->remove_templates)) {
            foreach (json_decode($template->remove_templates) as $rm_template) {
                $text = implode(preg_split($rm_template, $text));
            }
        }


        $clear_text = strip_tags($text);
        $short = "";
        $parts = explode(".", $clear_text);
        foreach ($parts as $part) {
            if (strlen($short) + strlen($part) > 150) {
                $short .= substr($part, 0, 150-strlen($short));
                break;
            }
            $short .= $part.'.';
        }
        $short.="...";


        $result = self::getDetector()->detect($clear_text)->close();
        $lang = array_keys($result)[0];
        $nw = new News();
        if (!$pic) {
            $pic = "/img/397x300/02.jpg";
        }
        $nw->pic = $pic;
        $nw->short= $short;
        $nw->title = $title;
        $nw->data = $text;
        $nw->original_urls = $url;
        $nw->lang = $lang;


        $nw->save();

        foreach ($tagObjs as $tag1) {

            $t_nw = new TagsToNews();
            $t_nw->t_id = $tag1->id;
            $t_nw->n_id = $nw->id;
            $t_nw->save();
        }

        $t_nw = new TemplatesToNews();
        $t_nw->t_id = $template->id;
        $t_nw->n_id = $nw->id;
        $t_nw->save();


        self::$tagVals = null;
        self::$tagCount = null;
    }


}