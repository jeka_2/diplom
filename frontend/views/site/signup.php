<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth-cnt text-center">
    <div class="site-signup">
        <h1><?= Yii::t("auth", "signup") ?></h1>


        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'email') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t("auth", "signup"), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
            </div>

            <div class="form-group">
                <h3><?= Yii::t("auth", "social_authorization") ?></h3>

                <?= yii\authclient\widgets\AuthChoice::widget([
                    'baseAuthUrl' => ['site/auth']
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>