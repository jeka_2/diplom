<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Contact List -->
<div class="section-seperator margin-b-0"  style="background: rgba(13,133,233,0.5)">
    <div class="content-lg container" >
        <div class="row">
            <!-- Contact List -->
            <div class="col-sm-4 sm-margin-b-50">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                </div>
            </div>
            <!-- End Contact List -->

            <!-- Contact List -->
            <div class="col-sm-4 sm-margin-b-50">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s" style="background:rgba(255,255,255,0.85); border-radius: 15px; padding:20px;">
                    <h3><a href="#"><?= Yii::t("main", "my_name")?></a> <span class="text-uppercase margin-l-20"><?= Yii::t("main", "my_role")?></span></h3>
                    <p><?= Yii::t("main", "my_legend")?></p>
                    <ul class="list-unstyled contact-list">
                        <li><i class="margin-r-10 color-base icon-call-out"></i> <?= Yii::$app->params["adminPhone"]?></li>
                        <li><i class="margin-r-10 color-base icon-envelope"></i> <?= Yii::$app->params["adminEmail"]?></li>
                    </ul>
                </div>
            </div>
            <!-- End Contact List -->

            <!-- Contact List -->
            <div class="col-sm-4 sm-margin-b-50">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">

                </div>
            </div>
            <!-- End Contact List -->
        </div>
        <!--// end row -->
    </div>
</div>
<!-- End Contact List -->



<!-- Promo Section -->
<div class="promo-section overflow-h">
    <div class="container">
        <div class="clearfix">
            <div class="ver-center">
                <div class="ver-center-aligned">
                    <div class="promo-section-col">
                        <h2><?= Yii::t("landing", "our_goal")?></h2>
                        <p><?= Yii::t("landing", "our_goal_text")?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="promo-section-img-right">
        <img class="img-responsive" src="img/970x970/01.jpg" alt="Content Image">
    </div>
</div>
<!-- End Promo Section -->

<script src="js/components/gmap.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBsXUGTFS09pLVdsYEE9YrO2y4IAncAO2U&amp;callback=initMap" async defer></script>
