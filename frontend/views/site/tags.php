<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SearchForm */
/* @var $renderLoader bool */
/* @var $tags \common\models\Tags[] */
/* @var $tag string|null */



$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;


?>

<section>


    <div class="tags-cnt">
        <div id="tags">
            <?php foreach($tags as $obj): ?>
                 <a href="<?= $obj->getUrl()?>" class="tag"><?= $obj->name?></a>
            <?php endforeach ?>

        </div>
    </div>
</section>


<section>

<?php if ($renderLoader): ?>
<h1 class="text-center"><?= $tag?></h1>
<div class="news_content" data-toggle="load_more" data-url="<?= \yii\helpers\Url::toRoute(["site/render-4-news"])?>" data-params='<?= $model->getParamsForLoader()?>' data-start="0"></div>

<?php endif;?>
</section>