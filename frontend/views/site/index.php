<?php

/* @var $this yii\web\View */
/* @var $news \common\models\News[] */


$block1_icons = ["icon-chemistry","icon-screen-tablet","icon-badge","icon-notebook","icon-speedometer","icon-badge"];

?>

<!-- Service -->
<div class="bg-color-sky-light" data-auto-height="true">
    <div class="content-lg container">
        <div class="row row-space-1 margin-b-2">
            <?php for ($id = 0; $id<6; $id++): ?>
            <div class="col-sm-4 sm-margin-b-2">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                    <div class="service" data-height="height">
                        <div class="service-element">
                            <i class="service-icon <?= $block1_icons[$id]?>"></i>
                        </div>
                        <div class="service-info">
                            <h3><?= Yii::t("landing", "block".($id+1)."_title")?></h3>
                            <p class="margin-b-5"><?= Yii::t("landing", "block".($id+1)."_text")?></p>
                        </div>
                        <a href="#" class="content-wrapper-link"></a>
                    </div>
                </div>
            </div>
            <?php endfor;?>


        </div>
        <!--// end row -->
    </div>
</div>
<!-- End Service -->

<!-- Latest Products -->
<div class="content-lg container">
    <div class="row margin-b-40">
        <div class="col-sm-6">
            <h2><?= Yii::t("landing", "last_news")?></h2>
            <p><?= Yii::t("landing", "last_news_text")?></p>
        </div>
    </div>
    <!--// end row -->

    <div class="row">


        <?php foreach($news as $article): ?>
        <!-- Latest Products -->
        <div class="col-sm-4 sm-margin-b-50">
            <div class="margin-b-20">
                <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                    <img class="img-responsive" src="<?=$article->pic?>" alt="Latest Products Image">
                </div>
            </div>
            <h4><a href="#"><?=$article->title?></a> <span class="margin-l-20">(<?= Yii::t("main", "lang")?>:<?= Yii::t("main", "lang_".$article->lang)?>)</span></h4>
            <p><?= $article->short?></p>
            <a class="link" href="#"><?= Yii::t("main", "read")?></a>
        </div>
        <!-- End Latest Products -->
        <?php endforeach; ?>
    </div>
    <!--// end row -->
</div>
<!-- End Latest Products -->

<!-- Testimonials -->
<div class="content-lg container margin-b-50">
    <div class="row">
        <div class="col-md-12 text-center margin-b-50">
            <h2><?= Yii::t("landing", "questions")?></h2>
        </div>
    </div>
        <?php for($i=1;$i<=4;$i++): ?>
            <?= $i%2==1?'<div class="row">':''?>
                <div class="col-md-6">


                    <blockquote class="blockquote">
                        <div class="margin-b-20">
                            <strong><?= Yii::t("landing", "question{$i}_q")?></strong>
                        </div>
                        <div class="margin-b-20">
                            <?= Yii::t("landing", "question{$i}_a")?>
                        </div>
                    </blockquote>

                    <!-- End Swiper Testimonials -->
                </div>

            <?= $i%2==0?'</div>':''?>
        <?php endfor; ?>
    <!--// end row -->
</div>
<!-- End Testimonials -->

<!-- Promo Section -->
<div class="promo-section overflow-h">
    <div class="container">
        <div class="clearfix">
            <div class="ver-center">
                <div class="ver-center-aligned">
                    <div class="promo-section-col">
                        <h2><?= Yii::t("landing", "our_goal")?></h2>
                        <p><?= Yii::t("landing", "our_goal_text")?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="promo-section-img-right">
        <img class="img-responsive" src="img/970x970/01.jpg" alt="Content Image">
    </div>
</div>
<!-- End Promo Section -->