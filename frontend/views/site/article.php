<?php

/**
 * @var $article \common\models\News
 * @var $this \yii\web\View
 */

?>
<div class="site-article">
        <div class="row margin-b-50">
            <div class="col-md-12 text-left sm-text-center">

                <?= \toriphes\lazyload\LazyLoad::widget(['src' => $article->pic, "options"=>["class"=>"article-img", "height"=>"200"]]);?>
                <h1><?=$article->title?></h1>
                <p><?= Yii::t("main","lang")?>:&nbsp;&nbsp;<span class="language-picker small text-danger"><i class="ru-RU"></i> <?= Yii::t("main", "lang_".$article->lang)?></span></p>
                <p><?= Yii::t("main","source_s")?>:&nbsp;&nbsp;<?php foreach($article->getSitesAsArray() as $source): ?> <a href="<?=$source?>"><span class="icon-anchor"></span> <?= $source?></a><?php endforeach?></p>
                <p><?= Yii::t("main","date")?>:&nbsp;&nbsp;<span class="icon-calendar text-success"> <?= date("h:i d.m.Y", $article->updated_at)?></span></p>
                <p><?= Yii::t("main","views")?>:&nbsp;&nbsp;<span class="icon-eye"></span> <?= $article->views?></p>
            </div>

        </div>

        <div class="row margin-b-20">

            <div class="col-md-12 margin-b-20 text-justify">
                <div class="text-justify">
                  <?= $article->data?>
                </div>
                <div class="text-right"><?php foreach($article->getTagsAsArray() as $source): ?> <a href="<?= \yii\helpers\Url::to(["site/tags", "tag"=>$source])?>"><span class="icon-tag"></span> <?= $source?></a><?php endforeach?></div>

            </div>


        </div>


</div>