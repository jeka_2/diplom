<?php
/**
 * @var $news \common\models\News[]
 * @var $this \yii\web\View
 * @var $start string
 */

$id = 2;
?>
<?php if (!sizeof($news)): ?>
    <div class="article-no-more">
        <h1 class="icon-note"><?= $start?Yii::t("news", "article_no_more"):Yii::t("news", "article_no_news")?></h1>
    </div>

<?php else: ?>
<div class="row" style="margin:15px;">
    <?php foreach ($news as $article): ?>
        <?php
            $url = \yii\helpers\Url::to(["site/article", "id"=>$article->id]);
            $id++;
            ?>
        <div class="col-md-6 search-article-block-col">

                    <div class="search-article-block">
                        <h3><a href="<?= $url?>"><?= $article->title?></a> <span class="text-uppercase margin-l-20"><?= Yii::t("main", "lang_".$article->lang)?></span> <span class="pull-right icon-calendar"> <?= date("h:i d.m.Y", $article->updated_at)?></span></h3>
                        <div style="margin: -15px 0 15px 0;"><?php foreach($article->getTagsAsArray() as $source): ?> <a href="<?= \yii\helpers\Url::to(["site/tags", "tag"=>$source])?>"><span class="icon-tag"></span> <?= $source?></a><?php endforeach?></div>
                        <p><?= Yii::t("main","source_s")?>:&nbsp;&nbsp;<?php foreach($article->getSitesAsArray() as $source): ?> <a href="<?=$source?>"><span class="icon-anchor"></span> <?= $source?></a><?php endforeach?></p>
                        <p class="text-justify" style="min-height:70px">
                            <img class="pull-left" style="margin:0 10px 10px 0;" alt="<?= $article->title?>" src="<?=$article->pic?>" height="80px"/>
                            <?= $article->short?>
                        </p>
                        <div class="text-right">
                            <a class="btn btn-warning text-uppercase" href="<?= $url?>"><?= Yii::t("main", "read")?></a>
                        </div>
                    </div>
        </div>
    <?php endforeach;?>
</div>

<?php endif; ?>

