<?php
/**
 * @var $news \common\models\News[]
 * @var $start string
 * @var $this \yii\web\View
 */

$id = 0;
?>
<?php if (!sizeof($news)): ?>
    <div class="article-no-more">
        <h1 class="icon-note"><?= $start?Yii::t("news", "article_no_more"):Yii::t("news", "article_no_news")?></h1>
    </div>

<?php else: ?>
<?php foreach ($news as $article): ?>
    <?php $url = \yii\helpers\Url::to(["site/article", "id"=>$article->id]);?>
    <?php switch(++$id): ?><?php case 1:?>
                <!-- Contact List -->
                <div class="section-seperator margin-b-0 news-block-1">
                    <div class="container" >
                        <div class="row">

                            <!-- Contact List -->
                            <div class="col-sm-8 col-sm-12 col-lg-6 col-lg-offset-3">
                                <div class="news-modal-1">
                                    <h3><a href="<?= $url?>"><?= $article->title?></a> <span class="text-uppercase margin-l-20"><?= Yii::t("main", "lang_".$article->lang)?></span> <span class="pull-right icon-calendar"> <?= date("h:i d.m.Y", $article->updated_at)?></span></h3>
                                    <div style="margin-top: -15px;"><?php foreach($article->getTagsAsArray() as $source): ?> <a href="<?= \yii\helpers\Url::to(["site/tags", "tag"=>$source])?>"><span class="icon-tag"></span> <?= $source?></a><?php endforeach?></div>
                                    <p><?= Yii::t("main","source_s")?>:&nbsp;&nbsp;<?php foreach($article->getSitesAsArray() as $source): ?> <a href="<?=$source?>"><span class="icon-anchor"></span> <?= $source?></a><?php endforeach?></p>
                                    <p class="text-justify" style="min-height:70px">
                                        <img class="pull-left" style="margin:0 10px 10px 0;" alt="<?= $article->title?>" src="<?=$article->pic?>" height="80px"/>
                                        <?= $article->short?>
                                    </p>
                                    <div class="text-right">
                                        <a class="btn btn-warning text-uppercase" href="<?= $url?>"><?= Yii::t("main", "read")?></a>
                                    </div>
                                </div>
                            </div>
                            <!-- End Contact List -->

                        </div>
                        <!--// end row -->
                    </div>
                </div>
                <!-- End Contact List -->

                <?php break;?>
                <?php case 2:?>

                <!-- Promo Section -->
                <div class="promo-section overflow-h">
                    <div class="container">
                        <div class="clearfix">
                            <div class="ver-center">
                                <div class="ver-center-aligned">
                                    <div class="promo-section-col">

                                        <h3><a href="<?= $url?>"><?= $article->title?></a> <span class="text-uppercase margin-l-20"><?= Yii::t("main", "lang_".$article->lang)?></span></h3>
                                        <div style="margin-top: -15px;"><?php foreach($article->getTagsAsArray() as $source): ?> <a href="<?= \yii\helpers\Url::to(["site/tags", "tag"=>$source])?>"><span class="icon-tag"></span> <?= $source?></a><?php endforeach?></div>


                                        <p><?= Yii::t("main","source_s")?>:&nbsp;&nbsp;<?php foreach($article->getSitesAsArray() as $source): ?> <a href="<?=$source?>"><span class="icon-anchor"></span> <?= $source?></a><?php endforeach?></p>

                                        <p class="text-justify" style="min-height:70px">
                                            <img class="pull-left" style="margin:0 10px 10px 0;" alt="<?= $article->title?>" src="<?=$article->pic?>" height="80px"/>
                                            <?= $article->short?>
                                        </p>
                                        <div class="text-right">
                                            <span class="icon-calendar pull-left" style="margin-top:10px"> <?= date("h:i d.m.Y", $article->updated_at)?></span>
                                            <a class="btn btn-warning text-uppercase" href="<?= $url?>"><?= Yii::t("main", "read")?></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- End Promo Section -->
                <?php break;?>
                <?php case 3:?>

                    <div class="promo-section-img-right">
                        <div style="position: relative;"><div style="position: absolute; padding:10%; left:0; top:0;">

                                <div class="news-modal-1">
                                    <h3><a href="<?= $url?>"><?= $article->title?></a> <span class="text-uppercase margin-l-20"><?= Yii::t("main", "lang_".$article->lang)?></span> <span class="pull-right icon-calendar"> <?= date("h:i d.m.Y", $article->updated_at)?></span></h3>
                                    <div style="margin-top: -15px;"><?php foreach($article->getTagsAsArray() as $source): ?> <a href="<?= \yii\helpers\Url::to(["site/tags", "tag"=>$source])?>"><span class="icon-tag"></span> <?= $source?></a><?php endforeach?></div>
                                    <p><?= Yii::t("main","source_s")?>:&nbsp;&nbsp;<?php foreach($article->getSitesAsArray() as $source): ?> <a href="<?=$source?>"><span class="icon-anchor"></span> <?= $source?></a><?php endforeach?></p>
                                    <p class="text-justify" style="min-height:70px">
                                        <img class="pull-left" style="margin:0 10px 10px 0;" alt="<?= $article->title?>" src="<?=$article->pic?>" height="80px"/>
                                        <?= $article->short?>
                                    </p>
                                    <div class="text-right">
                                        <a class="btn btn-warning text-uppercase" href="<?= $url?>"><?= Yii::t("main", "read")?></a>
                                    </div>
                                </div>
                            </div></div>
                        <img class="img-responsive" src="/img/970x970/01.jpg" alt="Content Image" />

                    </div>
                </div>
                <?php break;?>

    <?php endswitch;?>
<?php endforeach;?>

<?php endif; ?>

<?php if (sizeof($news) == 2):?>
    </div>
<?php endif; ?>