<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-cnt text-center">
    <div class="site-request-password-reset">
        <h1><?= Yii::t("auth", "reset_pass_page") ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>

            <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton(Yii::t("main", "send"), ['class' => 'btn btn-primary']) ?>
            </div>


        <?php ActiveForm::end(); ?>
    </div>
</div>
