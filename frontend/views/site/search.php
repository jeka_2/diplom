<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SearchForm */
/* @var $renderLoader bool */



$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;


?>

<section>

    <form method="GET">
    <div class="search-block">
        <table class="margin-b-20 full-width">
            <tr>
                <td class="full-width">
                    <input type="text" name="query" value="<?= $model->query?>" required class="form-control footer-input" placeholder="<?= Yii::t("main", "input_search_text")?>"/>
                </td>
                <td>

                    <button class="tn-theme btn-theme-sm btn-base-bg text-uppercase hidden-sm hidden-xs"><?= Yii::t("main", "search")?></button>
                </td>
            </tr>
        </table>
        <div class="row">

            <div class="col-md-4 margin-b-20">
                <input type="text" name="sites" value="<?= $model->sites?>" class="form-control footer-input" placeholder="<?= Yii::t("main", "input_site_text")?>"/>
            </div>
            <div class="col-md-8 margin-b-20">
                <input type="text" name="tags" value="<?= $model->tags?>" class="form-control footer-input" placeholder="<?= Yii::t("main", "input_tags_text")?>"/>
            </div>
        </div>

        <button class="tn-theme btn-theme-sm btn-base-bg text-uppercase hidden-lg hidden-md"><?= Yii::t("main", "search")?></button>

    </div>
    </form>

</section>


<?php if ($renderLoader): ?>

<div class="news_content" data-toggle="load_more" data-url="<?= \yii\helpers\Url::toRoute(["site/render-4-news"])?>" data-params='<?= $model->getParamsForLoader()?>' data-start="0"></div>

<?php endif;?>