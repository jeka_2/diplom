<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'О нас';
$this->params['breadcrumbs'][] = $this->title;


$block1_icons = ["icon-chemistry","icon-screen-tablet","icon-badge","icon-notebook","icon-speedometer","icon-badge"];

?>
<!-- Features -->
<div class="section-seperator">
    <div class="content-lg container">
        <div class="row">
            <?php for($i=1; $i<=3; $i++): ?>
            <div class="col-sm-4 sm-margin-b-50">
                <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                    <h3><?= Yii::t("about", "question{$i}_q")?></h3>
                    <p><?= Yii::t("about", "question{$i}_a")?></p>
                    <a class="link" href="#">Read More</a>
                </div>
            </div>
            <?php endfor; ?>
        </div>
        <!--// end row -->
    </div>
</div>
<!-- End Features -->

<!-- About -->
<div class="content-lg container">
    <div class="row margin-b-20">
        <div class="col-sm-6">
            <h2><?= Yii::t("about", "about")?></h2>
        </div>
    </div>
    <!--// end row -->

    <div class="row">
        <div class="col-sm-7 sm-margin-b-50">
            <div class="margin-b-30">
                <p><?= Yii::t("about", "about_text")?></p>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1">
            <img class="img-responsive" src="img/640x380/01.jpg" alt="Our Office">
        </div>
    </div>
    <!--// end row -->
</div>
<!-- End About -->

<!-- Service -->
<div class="bg-color-sky-light" data-auto-height="true">
    <div class="content-lg container">
        <div class="row row-space-1 margin-b-2">
            <?php for ($id = 0; $id<6; $id++): ?>
                <div class="col-sm-4 sm-margin-b-2">
                    <div class="wow fadeInLeft" data-wow-duration=".3" data-wow-delay=".3s">
                        <div class="service" data-height="height">
                            <div class="service-element">
                                <i class="service-icon <?= $block1_icons[$id]?>"></i>
                            </div>
                            <div class="service-info">
                                <h3><?= Yii::t("about", "block".($id+1)."_title")?></h3>
                                <p class="margin-b-5"><?= Yii::t("about", "block".($id+1)."_text")?></p>
                            </div>
                            <a href="#" class="content-wrapper-link"></a>
                        </div>
                    </div>
                </div>
            <?php endfor;?>


        </div>
        <!--// end row -->
    </div>
</div>
<!-- End Service -->