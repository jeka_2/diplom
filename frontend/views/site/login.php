<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('titles', 'site/login');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="auth-cnt text-center">
    <div class="site-login">
        <h1><?= Yii::t("auth", "login") ?></h1>

        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

            <?= $form->field($model, 'usernameOrEmail')->textInput(['autofocus' => true]) ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= $form->field($model, 'rememberMe')->checkbox() ?>

            <div style="color:#999;margin:1em 0">
                <?= Html::a(Yii::t('auth', 'reset_pass'), ['site/request-password-reset']) ?>?
            </div>

            <div class="form-group">
                <?= Html::submitButton(Yii::t("auth", "login"), ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
            </div>

            <div class="form-group">
                <h3><?= Yii::t("auth", "social_authorization") ?></h3>

                <?= yii\authclient\widgets\AuthChoice::widget([
                    'baseAuthUrl' => ['site/auth']
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>