<?php

use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 */

$this->title = Yii::t("titles", Yii::$app->controller->id . "/" . Yii::$app->controller->action->id);
\frontend\assets\IndexAsset::register($this);
?>

<?php Yii::$app->controller->trigger(\frontend\controllers\SiteController::EVENT_GET_FEED_MODEL);?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<!-- ==============================
    Project:        Metronic "Asentus" Frontend Freebie - Responsive HTML Template Based On Twitter Bootstrap 3.3.4
    Version:        1.0
    Author:         KeenThemes
    Primary use:    Corporate, Business Themes.
    Email:          support@keenthemes.com
    Follow:         http://www.twitter.com/keenthemes
    Like:           http://www.facebook.com/keenthemes
    Website:        http://www.keenthemes.com
    Premium:        Premium Metronic Admin Theme: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
================================== -->
<html lang="<?= Yii::$app->language ?>" class="no-js">
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <?php $this->head() ?>

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.ico"/>
    </head>
    <!-- END HEAD -->

    <!-- BODY -->
    <body>

    <?php $this->beginBody() ?>
        <header class="header navbar-fixed-top">
            <?= $this->render('parts/_menu_top')?>
        </header>


        <?= $this->render('parts/_slider')?>

         <?= $content ?>

        <?= $this->render('parts/_footer', ['feedBackModel'=>Yii::$app->view->params['feedBackModel']])?>

        <?= $this->render('parts/_to_top')?>

    <?php $this->endBody() ?>
    </body>
    <!-- END BODY -->
</html>

<?php $this->endPage() ?>