<nav class="navbar" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="menu-container">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="toggle-icon"></span>
            </button>

            <!-- Logo -->
            <div class="logo">
                <a class="logo-wrap inline-block-top" href="<?= \yii\helpers\Url::to(['site/index'])?>">
                    <img class="logo-img logo-img-main" src="/img/logo.png" alt="Asentus Logo">
                    <img class="logo-img logo-img-active" src="/img/logo-dark.png" alt="Asentus Logo">
                </a>
                <div class="inline-block-top lang-block">
                    <?= \lajax\languagepicker\widgets\LanguagePicker::widget([
                        'skin' => \lajax\languagepicker\widgets\LanguagePicker::SKIN_BUTTON,
                        'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_SMALL
                    ]); ?>
                </div>

            </div>
            <!-- End Logo -->
        </div>


        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse nav-collapse">
            <div class="menu-container">
                <ul class="navbar-nav navbar-nav-right text-center">
                    <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/news'])?>"><?= Yii::t("main", "news")?></a></li>
                    <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/tags'])?>"><?= Yii::t("main", "tags")?></a></li>
                    <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/search'])?>"><?= Yii::t("main", "search")?></a></li>
                    <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/about'])?>"><?= Yii::t("main", "about")?></a></li>
                    <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/contact'])?>"><?= Yii::t("main", "contacts")?></a></li>
                    <?php if(Yii::$app->user->isGuest): ?>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/log-in'])?>"><?= Yii::t("main", "logIn")?></a></li>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/sign-up'])?>"><?= Yii::t("main", "signUp")?></a></li>
                    <?php else: ?>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/cabinet'])?>"><?= Yii::t("main", "profile")?></a></li>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= \yii\helpers\Url::to(['site/log-out'])?>"><?= Yii::t("main", "logOut")?></a></li>
                    <?php endif; ?>

                </ul>
            </div>
        </div>
        <!-- End Navbar Collapse -->
    </div>
</nav>