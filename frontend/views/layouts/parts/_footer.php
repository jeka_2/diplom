<?php
/**
 * @var $feedBackModel \common\models\Feedback
 */

?>
<!--========== FOOTER ==========-->
<footer class="footer">
    <!-- Links -->
    <div class="footer-seperator">
        <div class="content-lg container">
            <div class="row">
                <div class="col-sm-2 sm-margin-b-50">
                    <!-- List -->
                    <ul class="list-unstyled footer-list">
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/index'])?>"><?= mb_convert_case(Yii::t("main", "main"),  MB_CASE_TITLE, 'UTF-8')?></a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/news'])?>"><?= mb_convert_case(Yii::t("main", "news"),  MB_CASE_TITLE, 'UTF-8')?></a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/tags'])?>"><?= mb_convert_case(Yii::t("main", "tags"),  MB_CASE_TITLE, 'UTF-8')?></a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/search'])?>"><?= mb_convert_case(Yii::t("main", "search"),  MB_CASE_TITLE, 'UTF-8')?></a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/about'])?>"><?= mb_convert_case(Yii::t("main", "about"),  MB_CASE_TITLE, 'UTF-8')?></a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="<?= \yii\helpers\Url::to(['site/contact'])?>"><?= mb_convert_case(Yii::t("main", "contacts"),  MB_CASE_TITLE, 'UTF-8')?></a></li>

                    </ul>
                    <!-- End List -->
                </div>
                <div class="col-sm-4 sm-margin-b-30">
                    <!-- List -->
                    <ul class="list-unstyled footer-list">
                        <li class="footer-list-item"><a class="footer-list-link" href="#">VK</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href="#">Facebook</a></li>
                    </ul>
                    <!-- End List -->
                </div>



                <div class="col-sm-5 sm-margin-b-30">
                    <?php $form = \yii\widgets\ActiveForm::begin(); ?>
                        <h2 class="color-white"><?= Yii::t("landing", "form_label")?></h2>
                        <?= $form->field($feedBackModel, 'name')->textInput(["class"=>"form-control footer-input margin-b-20",
                        "placeholder"=>Yii::t("landing", "form_name"), "required"=>true])->label(false) ?>
                        <?= $form->field($feedBackModel, 'email')->input("email", ["class"=>"form-control footer-input margin-b-20",
                            "placeholder"=>Yii::t("landing", "form_email"), "required"=>true])->label(false) ?>
                        <?= $form->field($feedBackModel, 'message')->textarea(["class"=>"form-control footer-input margin-b-30",
                            "placeholder"=>Yii::t("landing", "form_message"), "required"=>true,  "rows"=>"6", "style"=>"resize: none;"])->label(false) ?>

                    <button type="submit" class="btn-theme btn-theme-sm btn-base-bg text-uppercase"><?= Yii::t("main", "send")?></button>
                    <?php \yii\widgets\ActiveForm::end(); ?>
                </div>
            </div>
            <!--// end row -->
        </div>
    </div>
    <!-- End Links -->

    <!-- Copyright -->
    <div class="content container">
        <div class="row">
            <div class="col-xs-6">
                <img class="footer-logo" src="/img/logo.png" alt="Asentus Logo">
            </div>
            <div class="col-xs-6 text-right">
                <p class="margin-b-0"> Powered by: <?= Yii::t("main", "my_name")?></p>
            </div>
        </div>
        <!--// end row -->
    </div>
    <!-- End Copyright -->
</footer>
<!--========== END FOOTER ==========-->
