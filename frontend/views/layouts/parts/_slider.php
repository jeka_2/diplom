<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
    <div class="container">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        </ol>
    </div>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="img-responsive" src="img/1920x1080/01.jpg" alt="Slider Image">
            <div class="container">
                <div class="carousel-centered">
                    <div class="margin-b-40">
                        <h1 class="carousel-title"><?= Yii::t('landing', 'first_title')?></h1>
                        <p><?= Yii::t('landing', 'first_text')?></p>
                    </div>
                    <a href="#" class="btn-theme btn-theme-sm btn-white-brd text-uppercase"><?= Yii::t("landing", "first_button")?></a>
                </div>
            </div>
        </div>
        <div class="item">
            <img class="img-responsive" src="img/1920x1080/02.jpg" alt="Slider Image">
            <div class="container">
                <div class="carousel-centered">
                    <div class="margin-b-40">
                        <h1 class="carousel-title"><?= Yii::t('landing', 'second_title')?></h1>
                        <p><?= Yii::t('landing', 'second_text')?></p>
                    </div>
                    <a href="#" class="btn-theme btn-theme-sm btn-white-brd text-uppercase"><?= Yii::t("landing", "second_button")?></a>
                </div>
            </div>
        </div>
    </div>
</div>