<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=j002',
            'username' => 'phpmyadmin',//'j002_root',
            'password' => 'pass1234',//'j002_root_pass',
            'charset' => 'utf8',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'forceTranslation' => true,
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
//                    'sourceLanguage' => 'en-US',
                    'fileMap' => [
                        'app' => '*.php',
                        'app/error' => 'error.php',
                    ],
                ],
            ],
        ],
        'languagepicker' => [
            'class' => 'lajax\languagepicker\Component',        // List of available languages (icons and text)
            'languages' => array_keys(\common\helpers\TranslateHelper::getLanguages()),
            'cookieName' => 'language',                         // Name of the cookie.
            'expireDays' => 64,                                 // The expiration time of the cookie is 64 days.
            'callback' => function() {
                if (!\Yii::$app->user->isGuest) {
                    /**
                     * @var $user \common\models\User
                     */
                    $user = \Yii::$app->user->identity;
                    $user->lang = \Yii::$app->language;
                    $user->save();
                }
            }
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '175291756350121',
                    'clientSecret' => '01ba681f295326c219b1e2f6c74848ae',
                    'attributeNames' => ['name', 'email'],
                ],
                'vkontakte' => [
                    'class' => 'yii\authclient\clients\VKontakte',
                    'clientId' => '6172476',
                    'clientSecret' => 'AYhrEW4JBcjBuuS42vbK',
                    'attributeNames' => ['name', 'email'],
                ],
            ],
        ],



    ],
];
