<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ai".
 *
 * @property integer $id
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $tmn_id
 * @property integer $tmx_id
 * @property integer $times
 *
 * @property Tags $tmx
 * @property Tags $tmn
 */
class AI extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ai';
    }


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data'], 'string'],
            [['created_at', 'updated_at', 'tmn_id', 'tmx_id', 'times'], 'integer'],
            [['tmx_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tmx_id' => 'id']],
            [['tmn_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['tmn_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('fields', 'ID'),
            'data' => Yii::t('fields', 'Data'),
            'created_at' => Yii::t('fields', 'Created At'),
            'updated_at' => Yii::t('fields', 'Updated At'),
            'tmn_id' => Yii::t('fields', 'Tmn ID'),
            'tmx_id' => Yii::t('fields', 'Tmx ID'),
            'times' => Yii::t('fields', 'Times'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmx()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tmx_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTmn()
    {
        return $this->hasOne(Tags::className(), ['id' => 'tmn_id']);
    }
}
