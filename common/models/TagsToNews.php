<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags_to_news".
 *
 * @property integer $id
 * @property integer $t_id
 * @property integer $n_id
 *
 * @property News $n
 * @property Tags $t
 */
class TagsToNews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags_to_news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['t_id', 'n_id'], 'integer'],
            [['n_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::className(), 'targetAttribute' => ['n_id' => 'id']],
            [['t_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tags::className(), 'targetAttribute' => ['t_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('fields', 'ID'),
            't_id' => Yii::t('fields', 'T ID'),
            'n_id' => Yii::t('fields', 'N ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getN()
    {
        return $this->hasOne(News::className(), ['id' => 'n_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getT()
    {
        return $this->hasOne(Tags::className(), ['id' => 't_id']);
    }
}
