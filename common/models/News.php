<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $pic
 * @property string $lang
 * @property string $title
 * @property string $short
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $views
 * @property string $original_urls
 *
 * @property TagsToNews[] $tagsToNews
 * @property TemplatesToNews[] $templatesToNews
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pic'], 'required'],
            [['short', 'data'], 'string'],
            [['created_at', 'updated_at', 'views'], 'integer'],
            [['pic'], 'string', 'max' => 100],
            [['lang'], 'string', 'max' => 2],
            [['title'], 'string', 'max' => 255],
            [['original_urls'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('fields', 'ID'),
            'pic' => Yii::t('fields', 'Pic'),
            'lang' => Yii::t('fields', 'Lang'),
            'title' => Yii::t('fields', 'Title'),
            'short' => Yii::t('fields', 'Short'),
            'data' => Yii::t('fields', 'Data'),
            'created_at' => Yii::t('fields', 'Created At'),
            'updated_at' => Yii::t('fields', 'Updated At'),
            'views' => Yii::t('fields', 'Views'),
            'original_urls' => Yii::t('fields', 'Original Urls'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagsToNews()
    {
        return $this->hasMany(TagsToNews::className(), ['n_id' => 'id']);
    }

    public function getTagsAsArray() {
        $names = [];
        foreach ($this->tagsToNews as $obj) {
            $names []= $obj->t->name;
        }
        return $names;
    }


    public function getSitesAsArray() {
        $names = [];

        $urls = json_decode($this->original_urls);
        if (!is_array($urls)) {
            $urls = [$this->original_urls];
        }
        foreach ($urls as $obj) {
            $names []= $obj;
        }
        return $names;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplatesToNews()
    {
        return $this->hasMany(TemplatesToNews::className(), ['n_id' => 'id']);
    }
}
