<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "templates".
 *
 * @property integer $id
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $url
 * @property string $source_url
 * @property string $text_template
 * @property string $link_template
 * @property string $title_template
 * @property string $remove_templates
 * @property string $pic_template
 * @property string $tag_template
 * @property string $tags_template
 */
class Templates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'templates';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'remove_templates', 'pic_template'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['url', 'source_url', 'tag_template', 'tags_template', 'link_template', 'text_template', 'title_template'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('fields', 'ID'),
            'data' => Yii::t('fields', 'Data'),
            'created_at' => Yii::t('fields', 'Created At'),
            'updated_at' => Yii::t('fields', 'Updated At'),
            'url' => Yii::t('fields', 'Url'),
            'pic_template' => Yii::t('fields', 'Pic Template'),
            'source_url' => Yii::t('fields', 'Source Url'),
            'text_template' => Yii::t('fields', 'Text Template'),
            'link_template' => Yii::t('fields', 'Link Template'),
            'title_template' => Yii::t('fields', 'Title Template'),
            'remove_templates' => Yii::t('fields', 'Remove Templates'),
            'tag_template' => Yii::t('fields', 'Tag Template'),
            'tags_template' => Yii::t('fields', 'Tags Template'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplatesToNews()
    {
        return $this->hasMany(TemplatesToNews::className(), ['t_id' => 'id']);
    }
}
