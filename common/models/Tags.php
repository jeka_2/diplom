<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "tags".
 *
 * @property integer $id
 * @property string $name
 * @property integer $times
 * @property integer $created_at
 * @property integer $updated_at
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'times'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('fields', 'ID'),
            'name' => Yii::t('fields', 'Name'),
            'times' => Yii::t('fields', 'Times'),
            'created_at' => Yii::t('fields', 'Created At'),
            'updated_at' => Yii::t('fields', 'Updated At'),
        ];
    }

    /**
     * @return string
     */
    public function getUrl() {
        return Url::to(["site/tags", "tag"=>$this->name]);
    }
}
