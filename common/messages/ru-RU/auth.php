<?php

return [
    "login"=>"Вход",
    "signup"=>"Регистрация",
    "wrong_pass"=>"Неправильный логин, email или пароль",
    "reset_pass"=>"сбросить пароль",
    "reset_pass_page"=>"сброс пароля",
    "social_authorization"=>"или через социальные сети:"
];