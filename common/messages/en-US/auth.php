<?php

return [
    "login"=>"Loin",
    "signup"=>"Signup",
    "wrong_pass"=>"Wrong login, password or email",
    "reset_pass"=>"reset password",
    "reset_pass_page"=>"reset password",
    "social_authorization"=>"or through social networks:"
];