<?php

return [
    /***************************
     ***** QUESTIONS BLOCK *****
     **************************/
    "question1_q"=>"How to add a new site?",
    "question1_a"=>"Contact the administration via the 'contacts' page or leave a message at the bottom of the page",
    "question2_q"=>"Is it planning to add other languages?",
    "question2_a"=>"No",
    "question3_q"=>"How to add a new site?",
    "question3_a"=>"Contact the administration via the 'contacts' page or leave a message at the bottom of the page",

    /******************
     ***** ABOUT *****
     *****************/
    'about'=>'О нас',
    'about_text'=>'This site was created in order to facilitate the search and comparison of articles from various sources.
We respect the copyrights of our sources, so you can always go to the original page to read the full article',

    /******************
     ***** BLOCKS *****
     *****************/
    "block1_title"=>"Copyrights",
    "block1_text"=>"We do not steal articles, we only broadcast them",
    "block2_title"=>"Adaptability",
    "block2_text"=>"You can read articles on any device",
    "block3_title"=>"Base",
    "block3_text"=>"A large base of different opinions",
    "block4_title"=>"Links",
    "block4_text"=>"You can always go to the original article via link.",
    "block5_title"=>"Speed",
    "block5_text"=>"High download speed on weak devices",
    "block6_title"=>"Versatility",
    "block6_text"=>"We try to select sites for grap with opposite opinions.",
];