<?php

return [
    /**********************
     *****  GLoBAL  *******
     *********************/
    'siteName'=>'Thesis project',
    'my_name'=>'Evgenij	Koloskov',
    'my_role'=>'Student',
    'my_legend'=>'CS 13313',
    'source_s'=>'source',
    'date'=>'date',
    'views'=>'views',
    'create'=>'create',
    'lang'=>'language',
    'lang_ru'=>'russian',
    'lang_en'=>'engilsh',
    'lang_bg'=>'bulgarian',

    /**********************
     ******  Menu  ********
     *********************/
    'main'=>'main',
    'news'=>'news',
    'tags'=>'tags',
    'search'=>'serach',
    'about'=>'abou us',
    'contacts'=>'contacts',
    'logIn'=>'login',
    'signUp'=>'signup',
    'profile'=>'profile',
    'logOut'=>'logout',

    /**********************
     *****  BUTTONS  ******
     *********************/
    'read'=>'read',
    'send'=>'send',
    'to_top'=>'go top',
    'save'=>'save',


    /**********************
     *****  BUTTONS  ******
     *********************/
    "input_search_text"=>"Enter your request text",
    "input_site_text"=>"On sites (if necessary)",
    "input_tags_text"=>"Tags (if necessary, separated by commas)",
];