<?php

return [
    /******************
     ** SLIDER PAGES **
     *****************/
    "first_title"=>"SEARCH",
    "first_text"=>"Comprehensive search on various sites. Compare articles using AI.",
    "first_button"=>"SEARCH",
    "second_title"=>"NOTHING EXTRA",
    "second_text"=>"Read your favorite ad-free news",
    "second_button"=>"READ",
    /******************
     ***** BLOCKS *****
     *****************/
    "block1_title"=>"Copyrights",
    "block1_text"=>"We do not steal articles, we only broadcast them",
    "block2_title"=>"Adaptability",
    "block2_text"=>"You can read articles on any device",
    "block3_title"=>"Base",
    "block3_text"=>"A large base of different opinions",
    "block4_title"=>"Links",
    "block4_text"=>"You can always go to the original article via link.",
    "block5_title"=>"Speed",
    "block5_text"=>"High download speed on weak devices",
    "block6_title"=>"Versatility",
    "block6_text"=>"We try to select sites for grap with opposite opinions.",

    /***************************
     ***** LAST NEWS BLOCK *****
     **************************/
    "last_news"=>"Latest news",
    'last_news_text'=>'The latest articles',

    /***************************
     ***** QUESTIONS BLOCK *****
     **************************/
    "questions"=>"FAQ?",
    "question1_q"=>"How to add a new site?",
    "question1_a"=>"Contact the administration via the 'contacts' page or leave a message at the bottom of the page",
    "question2_q"=>"Is it planning to add other languages?",
    "question2_a"=>"No",
    "question3_q"=>"How to add a new site?",
    "question3_a"=>"Contact the administration via the 'contacts' page or leave a message at the bottom of the page",
    "question4_q"=>"Is it planning to add other languages?",
    "question4_a"=>"No",

    /***************************
     ***** OUR GOAL BLOCK ******
     **************************/
    "our_goal"=>"Our goal",
    "our_goal_text"=>"The purpose of this site is to make article search as convenient and simple as possible.",


    /***************************
     ******* FORM BLOCK ********
     **************************/
    "form_label"=>"Feedback",
    "form_name"=>"Name",
    "form_email"=>"Email",
    "form_message"=>"Message",
];