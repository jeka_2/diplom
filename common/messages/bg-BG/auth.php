<?php

return [
    "login"=>"Вход",
    "signup"=>"Регистрация",
    "wrong_pass"=>"Невалиден логин, email или парол",
    "reset_pass"=>"нова парола",
    "reset_pass_page"=>"нов парол",
    "social_authorization"=>"или чрез социални мрежи:"
];