<?php

namespace common\helpers;

class TranslateHelper {
    public static function getLanguages() {
        return [
            'ru-RU' => 'русский',
            'en-US' => 'english',
            'bg-BG' => 'български'
        ];
    }
}